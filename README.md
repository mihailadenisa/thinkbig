APLICAȚIE WEB PENTRU GESTIONAREA REZOLVĂRII BUG-URILOR


ROLURI:

Mătușoiu Adina Elena, Mihăilă Alexandra Denisa - Design, Front-End

Mihai Andreea, Mirea Corina Marina - Back-End

Tămăduianu Andreea Ana Maria - Creare structura și conectare la BD


RESPONSABIL DE PROIECT: 

Mihai Andreea - andreeamihai98@yahoo.com

RESPONSABIL DE PRODUS:

Tămăduianu Andreea Ana Maria - andreea.tamaduianu@gmail.com


FUNCȚIONALITĂȚI:

Aplicația web permite comunicarea dintre membri unei echipe a bug-urilor dintr-o aplicație.

Conectarea la aplicație se face printr-un cont bazat pe o adresă de email. 

Un membru în echipa unui proiect (MP) poate înregistra proiecte software, ce pot fi monotorizate prin aplicație.

Un tester (TST) al unui proiect poate înregistra un bug în aplicație.

Un MP poate vedea bug-urile înregistare pentru proiectele din care face parte și își poate aloca rezolvarea unui bug.

După rezolvarea unui bug, un MP poate adăuga un status al rezolvării.